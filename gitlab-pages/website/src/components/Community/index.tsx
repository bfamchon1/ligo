import LinkAsButton from "@site/src/components/Buttons/link-as-button";
import React from "react";
import styles from "./styles.module.css";

const Community = () => {
  return (
    <div className={styles["community"]}>
      <article className={styles["community__article"]}>
        <p id="contributors" className={styles["community__article-kpi"]}>
          78
        </p>
        <h3 className={styles["community__article-title"]}>contributors</h3>
        <p className={styles["community__article-description"]}>
          Ligo is open-sources and open for contributions. Join Ligo team on GitLab and start to
          contribute !
        </p>
        <LinkAsButton variant="ghost" href="">
          Contribute on GitLab
        </LinkAsButton>
      </article>
      <article className={styles["community__article"]}>
        <p id="discord-members" className={styles["community__article-kpi"]}>
          11253
        </p>
        <h3 className={styles["community__article-title"]}>members on discord</h3>
        <p className={styles["community__article-description"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua.
        </p>
        <LinkAsButton variant="ghost" href="">
          join our discord
        </LinkAsButton>
      </article>
      <article className={styles["community__article"]}>
        <p id="registry-projects" className={styles["community__article-kpi"]}>
          11
        </p>
        <h3 className={styles["community__article-title"]}>tutorials</h3>
        <p className={styles["community__article-description"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua.
        </p>
        <LinkAsButton variant="ghost" href="">
          See our registry
        </LinkAsButton>
      </article>
    </div>
  );
};

export default Community;
