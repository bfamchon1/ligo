import Title, { Subtitle } from "@site/src/components/Titles";
import clsx from "clsx";
import React from "react";
import styles from "./styles.module.scss";

type HomeSectionProps = { children: React.ReactNode; title: string; subtitle: string; id: string };

const HomeSection = ({ children, title, subtitle, id }: HomeSectionProps) => {
  return (
    <section id={id} className={clsx(styles.home__section, "reveal")}>
      <Title level={2}>{title}</Title>
      <Subtitle>{subtitle}</Subtitle>
      {children}
    </section>
  );
};

export default HomeSection;
